import asyncio
import concurrent.futures
import json
import sys


class StdioBackend:
    async def create(self, config):
        return StdioBackend()

    async def send(self, msgs):
        loop = asyncio.get_running_loop()

        for msg in msgs:
            with concurrent.futures.ThreadPoolExecutor() as pool:
                await loop.run_in_executor(pool,
                                           lambda: print(json.dumps(msg)))

    async def receive(self):
        loop = asyncio.get_running_loop()
        msg = ""
        with concurrent.futures.ThreadPoolExecutor() as pool:
            msg = await loop.run_in_executor(pool, sys.stdin.readline)

        return msg
