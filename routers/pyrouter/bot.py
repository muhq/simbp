import asyncio
import json
import re


class Bot:
    @classmethod
    async def create(cls, config):
        self = Bot()

        if 'Ids' in config:
            self.ids = config['Ids'].split(',')
        else:
            self.ids = '*'

        if 'Regex' in config:
            self.regex = re.compile(config['Regex'])

        self.proc = await asyncio.create_subprocess_exec(
            config['exec'],
            stdin=asyncio.subprocess.PIPE,
            stdout=asyncio.subprocess.PIPE,
        )
        return self

    def __init__(self):
        self.method_count = 0

    async def send(self, msgs):
        for msg in msgs:
            if self.ids != '*' and not msg.get('groupId', '') in self.ids:
                continue

            if hasattr(self, 'regex') and not re.match(self.regex,
                                                       msg.get('message', '')):
                continue

            rpc = {
                'method': 'receive',
                'jsonrpc': '2.0',
                'params': msg,
                'id': self.method_count
            }

            self.method_count += 1

            self.proc.stdin.write(f'{json.dumps(rpc)}\n'.encode('utf-8'))
            await self.proc.stdin.drain()

    async def receive(self):
        rpc = await self.proc.stdout.readline()
        rpc = rpc.decode('utf-8').strip()
        rpc = json.loads(rpc)

        # error
        if 'error' in rpc:
            # TODO handle errors
            pass

        # response
        if 'result' in rpc:
            return rpc['result']

        # invocation
        assert rpc['method'] == 'send'
        return rpc["params"]
