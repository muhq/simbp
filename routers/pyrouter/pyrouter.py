#!/usr/bin/env python3

import asyncio
import configparser
import importlib

from bot import Bot

BACKENDS = {}
BOTS = {}
PLUMBERS = {}


async def plumber(source, destinations):
    while True:
        msgs = await source.receive()

        if not isinstance(msgs, list):
            msgs = [msgs]

        for _, destination in destinations.items():
            print(f'plumbed {msgs} from {source} to {destination}')
            await destination.send(msgs)


async def main():
    config = configparser.ConfigParser()
    config.read('config.ini')

    for section in config.sections():
        if config[section]['type'] == 'Bot':
            BOTS[section] = await Bot.create(config[section])
        else:
            module = importlib.import_module(config[section]['Type'].lower())
            backend_class = getattr(module, config[section]['Type'])
            BACKENDS[section] = await backend_class.create(config[section])

    for name, backend in BACKENDS.items():
        PLUMBERS[name] = asyncio.create_task(plumber(backend, BOTS))

    for name, bot in BOTS.items():
        PLUMBERS[name] = asyncio.create_task(plumber(bot, BACKENDS))

    await asyncio.gather(*PLUMBERS.values())


if __name__ == '__main__':
    asyncio.run(main(), debug=True)
