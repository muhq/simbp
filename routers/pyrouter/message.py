import json


class Message:
    def __init__(self,
                 sender=None,
                 recipients=None,
                 message=None,
                 attachments=None,
                 messageId=None,
                 timestamp=None):
        self.sender = sender
        self.recipients = recipients or []
        self.message = message
        self.attachments = attachments or []
        self.messageId = messageId
        self.timestamp = timestamp

    def toJSON(self):
        return json.dumps(self.__dict__)

    def fromJSON(message):
        return Message(**json.loads(message))
