import asyncio


class IPBackend:
    async def serve(self, reader, writer):
        async def read():
            while True:
                data = {'sender': 'Alice', 'groupId': 'test'}
                data['message'] = (await
                                   reader.readline()).decode('utf-8').strip()
                await self.outqueue.put(data)

        async def write():
            while True:
                msgs = await self.inqueue.get()

                for msg in msgs:
                    s = f'@{msg["recipients"]}: {msg["message"]}'
                    writer.write(f"{s}\n".encode('utf-8'))
                    await writer.drain()

        asyncio.gather(read(), write())

    def __init__(self):
        self.outqueue = asyncio.Queue()
        self.inqueue = asyncio.Queue()

    @classmethod
    async def create(cls, data):
        self = IPBackend()
        self.server = await asyncio.start_server(self.serve,
                                                 data.get('ip', '127.0.0.1'),
                                                 data.get('port', 12345))
        asyncio.create_task(self.server.serve_forever())
        return self

    async def send(self, data):
        await self.inqueue.put(data)

    async def receive(self):
        return await self.outqueue.get()
