import ctypes
import subprocess
import signal

import dbussy as dbus

INTERFACE_NAME = "org.asamk.Signal"
PATH_NAME = "/org/asamk/Signal"


class SignalBackend:
    @classmethod
    async def create(cls, config):
        self = SignalBackend()

        # start signal-cli daemon
        self.signal_daemon = subprocess.Popen(
            ["signal-cli", "-u", config["user"], "daemon"],
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL,
            preexec_fn=lambda: ctypes.CDLL("libc.so.6").prctl(1, signal.SIGTERM))

        # connect to dbus
        bus_spec = config.get("bus_spec", "session")
        bus = {
            "session": dbus.DBUS.BUS_SESSION,
            "system": dbus.DBUS.BUS_SYSTEM
        }[bus_spec.lower()]

        self.conn = await dbus.Connection.bus_get_async(bus, private=False)
        self.conn.enable_receive_message({dbus.DBUS.MESSAGE_TYPE_SIGNAL})
        self.conn.bus_add_match("type=signal")

        return self

    async def send(self, msgs):
        def is_groupId(recipient):
            if not isinstance(recipient, list):
                return False
            for element in recipient:
                if not isinstance(element, int):
                    return False
            return True

        for data in msgs:
            objs = [data.get("message", ""), data.get("attachments", [])]

            recipients = data.get('recipients', [])
            if not isinstance(recipients, list) and not is_groupId(recipients):
                recipients = [recipients]

            for i, recipient in enumerate(recipients):
                # send to groups
                if is_groupId(recipient):
                    message = dbus.Message.new_method_call(
                        destination=INTERFACE_NAME,
                        path=PATH_NAME,
                        iface=INTERFACE_NAME,
                        method="sendGroupMessage")
                    message.append_objects("sasay", *(objs + [recipient]))
                    self.conn.send(message)

                    # remove group id from recipients
                    # TODO: check for change during iteration bugs
                    del recipients[i]

            # send to normal user ids
            if recipients:
                message = dbus.Message.new_method_call(
                    destination=INTERFACE_NAME,
                    path=PATH_NAME,
                    iface=INTERFACE_NAME,
                    method="sendMessage")

                objs.append(recipients)
                message.append_objects("sasas", *objs)

                self.conn.send(message)


    async def receive(self):
        while True:
            msg = await self.conn.receive_message_async()
            if msg.interface != INTERFACE_NAME or msg.path != PATH_NAME or msg.member != "MessageReceived":
                continue

            return {
                'timestamp': msg.all_objects[0],
                'sender': msg.all_objects[1],
                'groupId': msg.all_objects[2],
                'message': msg.all_objects[3],
                'attachments': msg.all_objects[4],
            }
