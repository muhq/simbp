# SIMBP - Simple Instant Messaging Bot Protocol

SIMBP is a simple json-rpc based protocol for instant messaging bots.

For the current draft see the [rfc](https://pad.hopfenspace.org/fischerbot?view).

This repository contains some bots as well as a simple python asyncio based
router implementation.

