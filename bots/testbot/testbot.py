#!/usr/bin/env python3

import asyncio
import concurrent.futures
import json
import sys

from jsonrpc import JSONRPCResponseManager, dispatcher


USAGE = """\
simbp test bot implementation

Usage: it responds to messages starting with '!' followed by a command.

Commands:
    * !help - print this help message
    * !echo [text] - respond with the received text 
    * !sayin <seconds> [text] - send text in after <seconds>
"""

SEND_ID = 0

@dispatcher.add_method
def receive(sender, groupId=None, message=None, attachments=None, messageId=None, timestamp=None):
    """Entry point for jsonrpc"""
    # we only handle text messages
    if not message or not message.startswith('!'):
        return []

    args = message.splitlines()[0].split()
    cmd = args[0][1:]

    recipients = [groupId] or [sender]
    ret_msg = {
        "recipients": recipients
    }

    if cmd == 'help':
        ret_msg["message"] = USAGE
    elif cmd == 'echo':
        ret_msg["message"] = ' '.join(args[1:])
    elif cmd == 'sayin':
        if len(args) < 2:
            ret_msg["message"] = f"ERROR: {' '.join(args)} not in form {cmd} <seconds> [text]."
        try:
            seconds = int(args[1])
        except ValueError as err:
            ret_msg["message"] = f"ValueError: {err}"
            return [ret_msg]

        async def say_in(seconds, text):
            global SEND_ID
            await asyncio.sleep(seconds)
            payload = {
                'method': 'send',
                'jsonrpc': '2.0',
                'id': SEND_ID,
                'params': {
                    'recipients': recipients,
                    'message': text,
                }
            }
            await outqueue.put(json.dumps(payload))
            SEND_ID += 1

        asyncio.create_task(say_in(seconds, ' '.join(args[2:])))
        return []
    else:
        ret_msg["message"] = "ERROR: unknown cmd. Enter !help for a list of commands."

    return [ret_msg]

async def main():
    global outqueue
    outqueue = asyncio.Queue()
    async def read():
        loop = asyncio.get_running_loop()
        while True:
            request = ""
            with concurrent.futures.ThreadPoolExecutor() as pool:
                request = await loop.run_in_executor(pool, sys.stdin.readline)

            response = JSONRPCResponseManager.handle(request.strip(), dispatcher)
            await outqueue.put(response.json)

    async def write():
        loop = asyncio.get_running_loop()
        while True:
            json_str = await outqueue.get()

            def write_msg():
                sys.stdout.write(json_str + '\n')
                sys.stdout.flush()

            with concurrent.futures.ThreadPoolExecutor() as pool:
                await loop.run_in_executor(pool, write_msg)

    await asyncio.gather(read(), write())


if __name__ == '__main__':
    asyncio.run(main())

